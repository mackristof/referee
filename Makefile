SHELL := /bin/bash
VERSION=`git describe --exact-match --tags HEAD 2> /dev/null`
COMMIT=`git rev-parse HEAD`
DATE_BUILD=`date +%Y-%m-%d\_%H:%M`

BIN_DIR := $(GOPATH)/bin
DEP := $(BIN_DIR)/dep
GOLINT := $(BIN_DIR)/golint

.PHONY: first
first: build

######################
## DEP
######################
$(DEP):
	go get -u github.com/golang/dep/cmd/dep

vendor: $(DEP) ## Install dependencies
	dep ensure

######################
## LINT
######################
$(GOLINT):
	mkdir -p $GOPATH/src/golang.org/x && git clone --depth 1 https://github.com/golang/lint.git $GOPATH/src/golang.org/x/lint && go get -u golang.org/x/lint/golint
	# go get -u github.com/golang/lint/golint

.PHONY: lint
lint: $(GOLINT) ## Start lint
	diff -u <(echo -n) <(gofmt -s -d ./src); [ $$? -eq 0 ]
	go tool vet -composites=false -shadow=true src/producer/*.go*
	go tool vet -composites=false -shadow=true src/referee/*.go
	diff -u <(echo -n) <(golint ./src/...); [ $$? -eq 0 ]

######################
## START stack
######################
.PHONY: start
start: build ## start benchmark
	./build/producer 8081 & \
	./build/producer 8082 & \
	./build/producer 8083 & \
	./build/producer 8084 & \
	./build/producer 8085 & \
	./build/referee http://localhost:8081/ http://localhost:8082/ http://localhost:8083/ http://localhost:8084/ http://localhost:8085/ & \




######################
## START vegeta
######################
.PHONY: benchmark
benchmark: stop start ## start benchmark
	echo "GET http://localhost:9000/" | vegeta attack -duration=30s | tee results.bin | vegeta report


######################
## STOP stack
######################
.PHONY: stop
stop:  ## stop benchmark
	killall producer & killall referee

######################
## BUILD
######################
.PHONY: build
build: vendor ## Build binary
	rm -rf build
	CGO_ENABLED=0 go build -ldflags "-extldflags '-static' -X main.version=${VERSION} -X main.commit=${COMMIT} -X main.date=${DATE_BUILD}" -o "build/producer" src/producer/main.go
	CGO_ENABLED=0 go build -ldflags "-extldflags '-static' -X main.version=${VERSION} -X main.commit=${COMMIT} -X main.date=${DATE_BUILD}" -o "build/referee" src/referee/main.go

######################
## CLEAN
######################
.PHONY: clean
clean: ## Remove vendors, previous build and doc temporary files
	rm -rf vendor
	rm -rf build

####################
## HELP
#####
.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'