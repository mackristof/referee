package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"math/rand"
	"net/http"
	"os"
	"time"
)

var (
	version string // set at build time
	commit  string // set at build time
	date    string // set at build time
	log     = logrus.WithField("logger", "producer")
	server  *http.Server
)

func main() {
	if len(os.Args) > 1 && os.Args[1] == "version" {
		fmt.Printf("version:  %s\nbuild: %s\ndate: %s\n", version, commit, date)
	} else {
		startServer(os.Args[1])

	}
}

func handlerfunc(w http.ResponseWriter, r *http.Request) {
	returnedValue := random(1, 10)
	fmt.Fprintf(w, "%d", returnedValue)
	waitingTime := random(5, 100)
	wait(waitingTime)
}

func startServer(port string) {
	http.HandleFunc("/", handlerfunc)
	server = &http.Server{
		Addr:    ":" + port,
		Handler: http.DefaultServeMux,
	}
	log.Fatal(server.ListenAndServe())
}

func wait(duration int) {
	time.Sleep(time.Duration(duration) * time.Millisecond)
}

func random(min, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min) + min
}
