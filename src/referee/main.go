package main

import (
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	//_ "net/http/pprof"
	"context"
	"io/ioutil"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"
)

var (
	version   string // set at build time
	commit    string // set at build time
	date      string // set at build time
	log       = logrus.WithField("logger", "referee")
	server    *http.Server
	producers []string
)

type response struct {
	emitter  string
	duration int64
	value    int
}

type request struct {
	r        *http.Request
	response chan []byte
}

func main() {
	version := flag.Bool("version", false, "a string")
	flag.Parse()

	if *version {
		fmt.Printf("version:  %s\nbuild: %s\ndate: %s\n", version, commit, date)
	} else {
		initProducers()
		requests := make(chan request, 100)
		go startServer(requests)
		go process(requests)
		ch := make(chan os.Signal)
		signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
		log.Println(<-ch)
	}
}

func initProducers() {
	producers = os.Args[1:]
	fmt.Printf("start with producers %v", producers)
}

func startServer(rq chan request) {

	http.HandleFunc("/", handle(rq))
	http.ListenAndServe(":9000", nil)
}

func handle(rq chan request) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if len(rq) < cap(rq) {
			nr := newRequest(r)
			rq <- nr
			w.Write(<-nr.response)
		} else {
			w.WriteHeader(http.StatusNoContent)
			w.Write([]byte("Too Busy :("))

		}
	}
}

func newRequest(r *http.Request) request { return request{r, make(chan []byte)} }

func process(rq chan request) {
	for r := range rq {
		r.requestProducers()
	}
}

func (r request) requestProducers() {
	var wg sync.WaitGroup
	producersResult := make(chan response, len(producers))
	for _, producerPort := range producers {
		wg.Add(1)
		go func(respChan chan response, url string) {
			defer wg.Done()
			// call http resp <- http.response.value
			ctx, cancel := context.WithTimeout(context.Background(), 50*time.Millisecond)
			defer cancel()
			respChan <- callProducer(ctx, url)
		}(producersResult, producerPort)
	}
	wg.Wait()
	close(producersResult)
	replies := "[ "
	for res := range producersResult {
		reply := "{ \"emitter\" : " + res.emitter + " , \"value\": " + string(res.value) + " }"
		replies = replies + reply + ", "
	}
	replies = replies[0:len(replies)-2] + "]"
	r.response <- []byte(replies)
}
func callProducer(ctx context.Context, url string) response {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Errorf("%v", err)
		return response{emitter: url, value: 99}
	}
	req = req.WithContext(ctx)
	client := http.DefaultClient
	start := time.Now()
	res, err := client.Do(req)
	if err != nil {
		//log.Errorf("%v", err)
		return response{emitter: url, value: 99}
	}
	if res.StatusCode == http.StatusOK {

		r, err := ioutil.ReadAll(res.Body)
		if err != nil {
			//log.Errorf("%v", err)
			return response{emitter: url, value: 99}
		}
		i, err := strconv.Atoi(string(r))
		if err != nil {
			//log.Errorf("%v", err)
			return response{emitter: url, value: 99}
		}
		return response{emitter: url, value: i, duration: time.Since(start).Nanoseconds()}
	}
	return response{emitter: url, value: 99}

}
